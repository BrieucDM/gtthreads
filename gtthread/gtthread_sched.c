#include "gtthread.h"

/*timer handler*/
void gtthread_signal_handler(int signum)
{
	if (swapcontext(&current_thread->context,&sched_context) == -1)
			error_handler("scheduler cannot swap context");	
}

/* starting routine */ 
void gtthread_starting_routine (void *(*start_routine)(void *),void *arg)
{
	void* c;
	c = (*start_routine)(arg);
	timer_block();
	if(current_thread->state != STOPPED)
	{
		current_thread->return_ptr = c;
		current_thread->state = STOPPED;
	}

	gtthread_yield();
}

/* scheduler routine */
int gtthread_scheduler(void)
{
	int i;
	while(1)
	{	
		timer_block();
		i=0;
		while(current_thread->prev->state == STOPPED)
		{
			i++;
			current_thread = current_thread->prev;
			if(i>identifier)/*no more runnable threads*/
				exit(0);
		}

		/* load the next thread node*/
		current_thread = current_thread->prev;
		timer_unblock();
		if (swapcontext(&sched_context,&current_thread->context) == -1)
			error_handler("scheduler cannot swap context");

	}
	
}

/*only for debug purposes*/
void display_queue (int nb)
{
	int i;
	gtthread_t thread = current_thread;
	for(i=0;i<nb;i++)
	{
		printf("thread %lu id %d state %d \n",(long)thread,thread->id,thread->state);
		thread = thread->next;
	}
	printf("\n");
}
	

