#include "gtthread.h"
/* see man pthread_mutex(3); except init does not have the mutexattr parameter,
 * and should behave as if mutexattr is NULL (i.e., default attributes); also,
 * static initializers do not need to be implemented */
int  gtthread_mutex_init(gtthread_mutex_t *mutex)
{
	gtthread_mutex_t mutex_ptr;	
	mutex_ptr = (gtthread_mutex_t)malloc(sizeof(struct gtthread_mutex));
	mutex_ptr->state = UNLOCKED;
	mutex_ptr->owner = -1;
	*mutex = mutex_ptr;
	return 0;
}

int  gtthread_mutex_lock(gtthread_mutex_t *mutex)
{	
	gtthread_mutex_t mutex_ptr =(gtthread_mutex_t) *mutex;
	if(current_thread->id != mutex_ptr->owner)
		while(mutex_ptr->state == LOCKED);
	mutex_ptr->owner = current_thread->id;
	mutex_ptr->state = LOCKED;
	return 0;

}

int  gtthread_mutex_unlock(gtthread_mutex_t *mutex)
{	
	gtthread_mutex_t mutex_ptr = (gtthread_mutex_t) *mutex;
	if(current_thread->id == mutex_ptr->owner)
		{
		mutex_ptr->state = UNLOCKED;
		return 0;
		}
	return 1;	
}
