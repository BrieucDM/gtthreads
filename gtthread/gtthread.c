#include "gtthread.h"

/* Must be called before any of the below functions. Failure to do so may
 * result in undefined behavior. 'period' is the scheduling quantum (interval)
 * in microseconds (i.e., 1/1000000 sec.). */


void gtthread_init(long period){

	gtthread_t thread_ptr;
	struct sigaction action;
	struct itimerval sched_timer;
	ucontext_t main_context;
	identifier = 0;
	
	/*creating the scheduler context*/
	memset (&sched_context, 0, sizeof (ucontext_t));
	if (getcontext(&sched_context) == -1)
		error_handler("cannot get main thread context");
	 
	sched_context.uc_stack.ss_sp = malloc(SIGSTKSZ);
  	sched_context.uc_stack.ss_size = SIGSTKSZ;
	sched_context.uc_stack.ss_flags = 0;
  	sched_context.uc_link = 0;
  	makecontext(&sched_context, (void (*) (void))gtthread_scheduler, 0);
	
	
	/*register main as a thread*/ 
	memset (&main_context, 0, sizeof (ucontext_t));
	if (getcontext(&main_context) == -1)
		error_handler("cannot get main thread context");

	thread_ptr = malloc(sizeof(struct gtthread));
	memset (thread_ptr, 0, sizeof (struct gtthread));
    	thread_ptr->prev = thread_ptr;
	thread_ptr->next = thread_ptr;
	thread_ptr->id = identifier;
	thread_ptr->joiner_id = -1;
	thread_ptr->state = STARTED;
	thread_ptr->context = main_context;
	thread_ptr->return_ptr = NULL;

	/*main is now registered in the scheduler list*/
	current_thread = thread_ptr;


	/*initialize the timer*/
 	memset (&action, 0, sizeof (struct sigaction));
 	action.sa_handler = &gtthread_signal_handler;
 	sigaction (SIGVTALRM, &action, NULL);

 	sched_timer.it_value.tv_sec = 0;
 	sched_timer.it_value.tv_usec = period;
 	sched_timer.it_interval.tv_sec = 0;
 	sched_timer.it_interval.tv_usec = period;
 	setitimer (ITIMER_VIRTUAL, &sched_timer, NULL);

	/*initialize the timer blocker*/
 	if ((sigemptyset(&intmask) == -1) || (sigaddset(&intmask, SIGVTALRM) == -1))
   		error_handler("Failed to initialize the signal mask");  
  

}

/* see man pthread_create(3); the attr parameter is omitted, and this should
 * behave as if attr was NULL (i.e., default attributes) */
int  gtthread_create(gtthread_t *thread,
                     void *(*start_routine)(void *),
                     void *arg)
{

	gtthread_t thread_ptr = (gtthread_t) &thread;
	ucontext_t thread_context;
	identifier ++;
	/* creating the thread context*/

	memset (&thread_context, 0, sizeof (ucontext_t ));

	if (getcontext(&thread_context) == -1)
 	error_handler("cannot get context");
  		thread_context.uc_stack.ss_sp = malloc(SIGSTKSZ);
  		thread_context.uc_stack.ss_size = SIGSTKSZ;
		thread_context.uc_stack.ss_flags = 0;
  		thread_context.uc_link = &current_thread->context;
		makecontext(&thread_context, (void (*) (void))gtthread_starting_routine, 2,(void (*) (void))start_routine, arg);

	/*creating the trhead node*/

	thread_ptr = (gtthread_t)malloc(sizeof(struct gtthread));
	thread_ptr->next = current_thread->next;
    	thread_ptr->prev = current_thread;
	thread_ptr->id = identifier;
	thread_ptr->joiner_id = -1;
	thread_ptr->state = STARTED;
	thread_ptr->context = thread_context;
	thread_ptr->return_ptr = NULL;
	

 	/*registering the thread in the scheduler queue*/
	timer_block();
	current_thread->next = thread_ptr;
	thread_ptr->next->prev = thread_ptr;
	*thread = thread_ptr;
	timer_unblock();

	return 0;

}

/* see man pthread_join(3) */
int  gtthread_join(gtthread_t thread, void **status)
{
	if(thread->joiner_id == -1)/*check if the thread isn't already joined*/
		{
		if(current_thread->joiner_id != thread->id || thread->return_ptr == (void*)PTHREAD_CANCELED)/*check for a potential deadlock*/
			{
			thread->joiner_id = current_thread->id;
			while(thread->state != STOPPED);
			timer_block();
			if(status != NULL)
				*status = thread->return_ptr;
			thread->prev->next = thread->next;
			free(thread);
			timer_unblock();
			return 0;
			}
		else
			return EDEADLK;
		}
	else
		return EINVAL;

}


/* see man pthread_exit(3) */
void gtthread_exit(void *retval)
{	
	timer_block();
	current_thread->return_ptr = retval;
	current_thread->state = STOPPED;
	timer_unblock();

	if(current_thread->id == 0)
		gtthread_yield();
}

/* see man pthread_self(3) */
gtthread_t gtthread_self(void)
{
	return current_thread;
}

/*yield scheduler*/
int gtthread_yield(void)
{
	gtthread_signal_handler(0);
	return 0;
}

/*equal operator*/
int gtthread_equal(gtthread_t t1,gtthread_t t2)
{
	if(t1->id == t2->id)
	{
		return 1;
	}
	return 0;
}

/* see man pthread_cancel(3) */
int gtthread_cancel(gtthread_t thread)
{	
	
	if(thread->state == STARTED)/*prevents from canceling a dead thread*/
	{
		timer_block();
		thread->return_ptr = (void*)PTHREAD_CANCELED;
		thread->state = STOPPED;
		timer_unblock();
		if(thread->id == current_thread->id)
			gtthread_yield();
		return 0;
	}
	else
		return 0;
}

/* Error handler*/
void  error_handler(char *msg)
{ 
	perror(msg); 
	exit(EXIT_FAILURE); 
}

/*Block the timer while acessing critical area*/
void timer_block (void)
{
	if (sigprocmask(SIG_BLOCK, &intmask, NULL) == -1)
		error_handler("Failed to block the signal"); 
}

/*Unblock the timer while leaving critical area*/
void timer_unblock (void)
{
	if (sigprocmask(SIG_UNBLOCK, &intmask, NULL) == -1)
		error_handler("Failed to block the signal\n"); 
}
