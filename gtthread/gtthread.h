#ifndef __GTTHREAD_H
#define __GTTHREAD_H

#include <ucontext.h>
#include <string.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>

#define EINVAL 22
#define EDEADLK 35
#define STARTED 1
#define LOCKED  1
#define UNLOCKED 2
#define STOPPED 2
#define PTHREAD_CANCELED -1



/* Nodes of the liked chain of threads*/
typedef struct gtthread {
	int id;/*unique thread identifier*/
	int joiner_id;/*if the thread is joined this is the id of the joiner*/
	char state;/*STARTED and STOPPED*/
	void*	return_ptr;/*here is stored the return value*/
	struct gtthread *prev;
	struct gtthread *next;
	ucontext_t context;
} gtthread, *gtthread_t;

typedef struct gtthread_mutex {
	char state;
	int owner;/*id of the thread that locked the mutex*/
}gtthread_mutex,*gtthread_mutex_t;


/*global variables used by the library*/
gtthread_t current_thread;
ucontext_t sched_context;
sigset_t intmask;
int identifier;

/* Must be called before any of the below functions. Failure to do so may
 * result in undefined behavior. 'period' is the scheduling quantum (interval)
 * in microseconds (i.e., 1/1000000 sec.). */
void gtthread_init(long period);

/* see man pthread_create(3); the attr parameter is omitted, and this should
 * behave as if attr was NULL (i.e., default attributes) */
int  gtthread_create(gtthread_t *thread,
                     void *(*start_routine)(void *),
                     void *arg);

/* see man pthread_join(3) */
int  gtthread_join(gtthread_t thread, void **status);

/* gtthread_detach() does not need to be implemented; all threads should be
 * joinable */

/* see man pthread_exit(3) */
void gtthread_exit(void *retval);


/* see man pthread_self(3) */
gtthread_t gtthread_self(void);

/* see man sched_yield(2) */
int gtthread_yield(void);

/* see man pthread_equal(3) */
int  gtthread_equal(gtthread_t t1, gtthread_t t2);

/* see man pthread_cancel(3); but deferred cancelation does not need to be
 * implemented; all threads are canceled immediately */
int  gtthread_cancel(gtthread_t thread);

/* see man pthread_mutex(3); except init does not have the mutexattr parameter,
 * and should behave as if mutexattr is NULL (i.e., default attributes); also,
 * static initializers do not need to be implemented */
int  gtthread_mutex_init(gtthread_mutex_t *mutex);

int  gtthread_mutex_lock(gtthread_mutex_t *mutex);

int  gtthread_mutex_unlock(gtthread_mutex_t *mutex);

/* gtthread_mutex_destroy() and gtthread_mutex_trylock() do not need to be
 * implemented */

/* Error handler*/
void  error_handler(char *msg);

/* timer handler*/
void gtthread_signal_handler(int signum);

/* starting routine, the thread is running inside it, it is used to get the return value and update the thread state*/ 
void gtthread_starting_routine (void *(*start_routine)(void *),void *arg);

/* scheduler routine*/
int gtthread_scheduler(void);

/*Debug function, display the current queue, nb indicate the number of threads to display (the queue is infinite ie a ring)*/
void display_queue (int nb);

/*Block the timer while acessing critical area*/
void timer_block (void);

/*UNBlock the timer while leaving critical area*/
void timer_unblock (void);

#endif
