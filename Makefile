#### GTThread Library Makefile

CFLAGS  = -Wall -pedantic
LFLAGS  =
CC      = gcc
RM      = /bin/rm -rf
AR      = ar rc
RANLIB  = ranlib





LIBRARY = gtthread.a 

LIB_SRC =  ./gtthread/gtthread.c ./gtthread/gtthread_sched.c ./gtthread/gtthread_mutex.c

# DEMO_SRC = ./dining_philosophers/main.c

LIB_OBJ = $(patsubst %.c,%.o,$(LIB_SRC))

# DEMO_OBJ = $(patsubst %.c,%.o,$(DEMO_SRC))

# pattern rule for object files
%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

all:  $(LIBRARY) main

main : 
	gcc ./dining_philosophers/main.c -o main gtthread.a

$(LIBRARY): $(LIB_OBJ)
	$(AR) $(LIBRARY) $(LIB_OBJ)
	$(RANLIB) $(LIBRARY)

clean:
	$(RM) $(LIBRARY) $(LIB_OBJ) main

.PHONY: depend
depend:
	$(CFLAGS) -- $(LIB_SRC)  2>/dev/null
