 #include "gtthread.h"

/*Brieuc du Maugouer*/
/* Dining philosophers problem, resource hierarchy solution (see wikipedia)*/

gtthread_mutex_t chopsticks[5];
gtthread_t	philosophers[5];

/* philosopher thread*/
void * philosopher(void* id)
{
	int r,i;
	printf("philosopher %lu ready \n",(long)id);

	srand(time(NULL));

	/*each philosopher acquire the available chopstick with the lowest index
	for example if philosopher 4 can take 0 and 4, he'll take 0*/
	while(1)
		{
		if((long)id == 4)
			gtthread_mutex_lock(&chopsticks[0]);
		else
			gtthread_mutex_lock(&chopsticks[(long)id]);	

		/* at this point the philosopher has it's first chopstick, one more an he can eat*/
		printf("Philosopher %lu is waiting for a second chopstick \n",(long)id);

		if((long)id == 4)
			gtthread_mutex_lock(&chopsticks[4]);
		else
			gtthread_mutex_lock(&chopsticks[(long)id+1]);

		printf("Philosopher %lu is eating\n",(long)id);

		r= rand();
		for(i = 0; i < r; ++i);

		if((long)id == 4)
			{
			gtthread_mutex_unlock(&chopsticks[4]);
			gtthread_mutex_unlock(&chopsticks[0]);
			}
		else	
			{
			gtthread_mutex_unlock(&chopsticks[(long)id]);
			gtthread_mutex_unlock(&chopsticks[(long)id+1]);
			}

		printf("Philosopher %lu is thinking\n",(long)id);

		r= rand();
		for(i = 0; i < r; ++i);

		gtthread_yield();

		}
	

}


int main(void)
{
	long i; 

	gtthread_init(1000);

	/*initialize the mutexes*/
	for(i=0;i<5;i++)
	{	
		gtthread_mutex_init(&chopsticks[i]);
		/*lock all the chopsticks prior to launching the threads*/
		gtthread_mutex_lock(&chopsticks[i]);
		
	}

	for(i=0;i<5;i++)
	{	
		if(gtthread_create(&philosophers[i],&philosopher,(void*)i) == 1)
			printf("error philosopher thread could not be created\n");
		
	}

	/*philosophers are now around the table, serve the dishes by unlocking the mutexes*/

	for(i=0;i<5;i++)
	{	
		gtthread_mutex_unlock(&chopsticks[i]);
		
	}

	while(1);

		
	return 0;
	
		
		
}

